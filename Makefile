diagrams = \
	Symmetric_NAT.svg \
	symmetric-shared.svg \
	symmetric-symmetric.svg \
	symmetric-unnatted.svg \
	unnatted-unnatted.svg \
	$(NULL)
tex_files = \
	presentation.tex \
	handout.tex \
	slides.tex \
	$(NULL)

all: source.tar.xz presentation.pdf handout.pdf

source.tar.xz: $(shell git ls-files)
	git archive HEAD | xz > $@

presentation.pdf: presentation.tex slides.tex $(diagrams)
	pdflatex --shell-escape $<
handout.pdf: handout.tex slides.tex $(diagrams)
	pdflatex --shell-escape $<

clean:
	rm -f $(tex_files:.tex=.aux) $(tex_files:.tex=.log)
	rm -f $(tex_files:.tex=.nav) $(tex_files:.tex=.out)
	rm -f $(tex_files:.tex=.snm) $(tex_files:.tex=.toc)
	rm -f $(tex_files:.tex=.vrb)
	rm -f $(diagrams:.svg=.pdf) $(diagrams:.svg=.pdf_tex)
	rm -f presentation.pdf handout.pdf
	rm -f source.tar.xz

.PHONY: all clean
